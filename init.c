#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <limits.h>

#define N 60

double const refValue = 2.5; // valeur à déterminer

int compare( const void* a, const void* b)
{
     double double_a = * ( (double*) a );
     double double_b = * ( (double*) b );

     if ( double_a == double_b ) return 0;
     else if ( double_a < double_b ) return -1;
     else return 1;
}

typedef struct {
  double t[N];
  int pos;
}slidingArray;

void initSA(slidingArray* sa){
  sa->pos = 0;
}

void addSA(slidingArray *sa, double x){
  sa->t[sa->pos] = x;
  sa->pos = (sa->pos + 1) % N;
}

double medianeSA(slidingArray *sa){
  double t[N];
    for(int i =0; i < N; ++i){
    t[i] = sa->t[i];
  }
  qsort(t, N, sizeof(double), compare);
  return t[(int)N/2];
}

int main(){
  srand(time(NULL));
  slidingArray sa1, sa2, sa3, sa4, test;
  initSA(&test);
  for(int i = 0; i < N; ++i){
    double b =  ((double)(rand() % 1000)/1000.0)*5.0;
    double c = 2.5 + b/10.0;
    double d = 2.5 - b/10.0;
    sa1.t[i] = b;
    sa2.t[i] = c;
    sa3.t[i] = d;
    sa4.t[i] = (double)rand();
    addSA(&test, b);
  }
  addSA(&test, 3.3);
  addSA(&test, 3.7);
  printf("%f", test.t[2]);
  double med = medianeSA(&sa1);
  return 0;
}


