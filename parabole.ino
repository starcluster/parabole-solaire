#define N 60

double const refValue = 2.5; // valeur à déterminer

int pinEngine = 7;
float thSun = 2.7;
float thTurn = 2.6;


int compare( const void* a, const void* b)
{
     double double_a = * ( (double*) a );
     double double_b = * ( (double*) b );

     if ( double_a == double_b ) return 0;
     else if ( double_a < double_b ) return -1;
     else return 1;
}

typedef struct slidingArray slidingArray;
struct slidingArray{
  double t[N];
  int pos;
};

void initSA(struct slidingArray* sa){
  sa->pos = 0;
}

void addSA(struct slidingArray *sa, double x){
  sa->t[sa->pos] = x;
  sa->pos = (sa->pos + 1) % N;
}

double medianeSA(struct slidingArray *sa){
  double t[N];
    for(int i =0; i < N; ++i){
    t[i] = sa->t[i];
  }
  qsort(t, N, sizeof(double), compare);
  return t[(int)N/2];
}

void setup() {
  Serial.begin(9600);

  pinMode(pinEngine, OUTPUT);
  digitalWrite(pinEngine, LOW);
}

void loop() {
  
  digitalWrite(pinEngine, HIGH);
  delay(100);
  digitalWrite(pinEngine, LOW);
  delay(1000);
  Serial.print("rotation\n");
  
  int sensorValueSun = analogRead(A0);
  float sunQuantity = sensorValueSun * (5.0/1023.0);

  int sensorValueDiff = analogRead(A1);
  float diffVolt = sensorValueDiff * (5.0/1023.0);

  if (sunQuantity > thSun){
    if (diffVolt > thTurn){
        digitalWrite(pinEngine, HIGH);
        //delay(10);
//        qsort();
//        Serial.print(
        digitalWrite(pinEngine, LOW);
    }
  }
}



int hyst(float x, float m, float h, int state){
  if (state and x < m - h){
    state = 0;
  }
  else if (!state and x > m - h){
    state = 1;
  }
  return state;
}

/* Todo
 * - tableau glissant
 * - phase d'initialisation: mis sur on, se met à tourner vérifie changement de luminosité et cherche une valeur de confiance pour thSun
 * si en tournant ça diminue on dit que les 60 valeurs téainet pas si mal on prend la médiane
 * - mettre un bouton qui dit à l'arduino de tourner 
 */
